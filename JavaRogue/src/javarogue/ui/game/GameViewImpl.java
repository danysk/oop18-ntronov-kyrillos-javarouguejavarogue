package javarogue.ui.game;

import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javarogue.config.ConfigGraphics;
import javarogue.level.Level;
import javarogue.ui.game.components.Camera;
import javarogue.ui.game.components.GUI;
import javarogue.ui.game.components.MiniMap;
import javarogue.utility.Position;
import javarogue.utility.Translation;

/**
 * <h1>GameControllerImpl</h1>
 * 
 * Implementation of {@link GameView}.
 *
 */
public class GameViewImpl implements GameView {

	private final int STAGE_HORZ_OFFSET = 6;
	private final int STAGE_VERT_OFFSET = 29;
	
	private GameController controller;
	private GraphicsContext context;
	private Stage stage;
	
	private Camera camera;
	private MiniMap minimap;
	private GUI gui;
	
	@Override
	public void setController(GameController controller) {
		this.controller = controller;
	}

	@Override
	public void render() {
		Level level;
		if (!this.controller.getCurrentLevel().isPresent()) {
			throw new IllegalStateException("Cannot render non-existing level!");
		} else {
			level = this.controller.getCurrentLevel().get();
		}
		this.context.clearRect(0, 0, this.stage.getWidth(), this.stage.getHeight());
		this.camera.setLevel(level);
		this.camera.draw();
		this.minimap.setLevel(level);
		this.minimap.draw();
		this.gui.draw();
	}

	@Override
	public void open() {
		// Make window
		this.stage = new Stage();
		this.stage.setScene(this.buildScene());
		this.stage.setFullScreen(ConfigGraphics.fullscreen());
		this.stage.setFullScreenExitHint("");
		this.stage.setResizable(false);
		this.stage.setTitle("JavaRogue");
		this.stage.getIcons().add(this.loadIcon());
		// Adding offset to stage size to adjust to JavaFX's counting window borders as part of the window.
		this.stage.setWidth(ConfigGraphics.resolutionWidth() + this.STAGE_HORZ_OFFSET);
		this.stage.setHeight(ConfigGraphics.resolutionHeight() + this.STAGE_VERT_OFFSET);
		this.stage.show();
		// Init components
		this.camera = new Camera(this.context);
		this.camera.setOrigin(new Position(0,0));
		this.minimap = new MiniMap(this.context);
		this.gui = new GUI(this.context, this.controller);
	}

	@Override
	public void close() {
		this.stage.close();
	}

	private Scene buildScene() {
		Canvas canvas = new Canvas(ConfigGraphics.resolutionWidth(), ConfigGraphics.resolutionHeight());
		this.context = canvas.getGraphicsContext2D();
		GridPane pane = new GridPane();
		pane.add(canvas, 0, 0);
		Scene scene = new Scene(pane);
		controller.generateLevels();
		scene.setOnKeyPressed(e -> {
			switch (e.getCode()) {
			case W:
			case UP:
				this.controller.getCurrentLevel().get().moveCharacter(Translation.UP);
				this.render();
				break;
			case S:
			case DOWN:
				this.controller.getCurrentLevel().get().moveCharacter(Translation.DOWN);
				this.render();
				break;
			case A:
			case LEFT:
				this.controller.getCurrentLevel().get().moveCharacter(Translation.LEFT);
				this.render();
				break;
			case D:
			case RIGHT:
				this.controller.getCurrentLevel().get().moveCharacter(Translation.RIGHT);
				this.render();
				break;
			case I:
				this.gui.changeInventoryState();
				this.render();
				break;
			case ESCAPE:
				this.stage.close();
				break;
			default:
			}
			this.render();
		});
		return scene;
	}
	
	private Image loadIcon() {
		try {
			Image icon = new Image(this.getClass().getResourceAsStream("/icon.png"));
			return icon;
		} catch (NullPointerException e) {
			System.err.println("Failed loading icon.png");
			return null;
		}
	}

}

