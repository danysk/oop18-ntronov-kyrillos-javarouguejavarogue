package javarogue.generation;

import javarogue.config.ConfigData;
import javarogue.generation.algorithms.BossGeneration;
import javarogue.generation.algorithms.GenerationContext;
import javarogue.generation.algorithms.SimpleGeneration;
import javarogue.level.Level;

/**
 * 
 * <h1>Level Factory</h1>
 * 
 * Implementation of LevelFactory.
 *
 */
public class LevelFactoryImpl implements LevelFactory {

	private GenerationContext generationContext;
	
	/**
	 * <h1>LevelFactoryImpl</h1>
	 * Construct a level factory with a generation seed.
	 * @param seed generation seed
	 * @see Level
	 */
	public LevelFactoryImpl() {
		this.generationContext = new GenerationContext();
	}
	
	@Override
	public Level generateSimpleLevel(int depth) {
		switch(depth) {
		case 0:
			this.generationContext.setGenerationAlgorithm(new SimpleGeneration(30, 8, ConfigData.seed()));
			break;
		case 1:
			this.generationContext.setGenerationAlgorithm(new SimpleGeneration(40, 10, ConfigData.seed()));
			break;
		case 2:
			this.generationContext.setGenerationAlgorithm(new SimpleGeneration(45, 12, ConfigData.seed()));
			break;
		default:
			throw new UnsupportedOperationException("Cannot generate Simple levels of depth lower than 0 or higher than 2.");
		}
		return this.generationContext.generateLevel();
	}

	@Override
	public Level generateBossLevel() {
		this.generationContext.setGenerationAlgorithm(new BossGeneration());
		return this.generationContext.generateLevel();
	}
	
}
