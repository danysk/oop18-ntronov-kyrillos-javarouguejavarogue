package javarogue.player;

import javarogue.utility.Position;

/**
 * <h1>Player</h1>
 * 
 * A class representing a player-controlled character for level exploration.
 *
 */
public class Player {

	private Position position;
	
	/**
	 * 
	 * @param position Player origin position
	 */
	public Player(Position position) {
		this.position = position;
	}
	
	/**
	 * 
	 * @return Player's position
	 */
	public Position getPosition() {
		return this.position;
	}
	
	/**
	 * Move player by delta-x and delta-y in x and y axis.
	 * @param dx delta-x
	 * @param dy delta-y
	 */
	public void move(int dx, int dy) {
		this.position.translate(dx, dy);
	}
	
}
