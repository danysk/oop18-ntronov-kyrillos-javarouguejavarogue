package javarogue.utility;

/**
 * <h1>Translation</h1>
 * 
 * Represents a translation on x and y axis by a unit.
 *
 */
public enum Translation {

	UP(-1, 0),
	DOWN(+1, 0),
	LEFT(0, -1),
	RIGHT(0, +1);

	private int dx;
	private int dy;

	private Translation(int dx, int dy) {
		this.dx = dx;
		this.dy = dy;
	}

	/**
	 * 
	 * @return translation on X axis
	 */
	public int getDx() {
		return this.dx;
	}

	/**
	 * 
	 * @return translation on y axis
	 */
	public int getDy() {
		return this.dy;
	}

}
