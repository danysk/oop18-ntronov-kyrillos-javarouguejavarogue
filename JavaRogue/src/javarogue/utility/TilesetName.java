package javarogue.utility;

public enum TilesetName {

	DEFAULT("Default"),
	ALT("Alt");
	
	private String name;
	
	private TilesetName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
}
